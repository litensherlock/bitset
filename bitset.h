#ifndef MY_BITS
#define MY_BITS

struct bitset{
	unsigned long * parts;
	int length;
	int number_of_usable_bits;
	};

struct bitset * create_bitset(int number_of_bits, int rand);
void free_bitset(struct bitset *);
void print_bitset(struct bitset *);
void set_bit(int idx, struct bitset * b);
void reset_bit(int idx, struct bitset * b);
int check_bit(int idx, struct bitset * b);
int popcount(struct bitset * b);
int popcount_sequential(struct bitset * b);
int find_lowest_set_bit(struct bitset * b);
int find_lowest_set_bit_sequential(struct bitset * b);
int find_highest_set_bit(struct bitset * b);
int find_highest_set_bit_sequential(struct bitset * b);
int find_second_lowest_set_bit(struct bitset * b);
struct bitset * reverse_bitset(struct bitset * b);
struct bitset * reverse_bitset_sequential(struct bitset * b);
void reverse_bitset_inplace(struct bitset * b);
void print_char(char a);
void reverse_bitset_sequential_inplace(struct bitset * b);

#endif