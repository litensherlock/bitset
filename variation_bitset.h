#ifndef MY_VARIATION_BITS
#define MY_VARIATION_BITS

#include "bitset.h"

int find_lowest_set_bit_binary_search_branching(struct bitset * b);
int popcount_1(struct bitset * b);
void reverse_bitset_variation(struct bitset * b);



#endif