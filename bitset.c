#include <stdlib.h>
#include "bitset.h"
#include <stdio.h>

#define byte_size 8

static const unsigned char BitReverseTable256[] =
{
  0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50,
0xD0, 0x30, 0xB0, 0x70, 0xF0, 0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68,
0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, 0x04, 0x84, 0x44,
0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74,
0xF4, 0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C,
0xDC, 0x3C, 0xBC, 0x7C, 0xFC, 0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62,
0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2, 0x0A, 0x8A, 0x4A,
0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A,
0xFA, 0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56,
0xD6, 0x36, 0xB6, 0x76, 0xF6, 0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E,
0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE, 0x01, 0x81, 0x41,
0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71,
0xF1, 0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59,
0xD9, 0x39, 0xB9, 0x79, 0xF9, 0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65,
0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5, 0x0D, 0x8D, 0x4D,
0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D,
0xFD, 0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53,
0xD3, 0x33, 0xB3, 0x73, 0xF3, 0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B,
0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB, 0x07, 0x87, 0x47,
0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77,
0xF7, 0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F,
0xDF, 0x3F, 0xBF, 0x7F, 0xFF };

struct bitset * create_bitset(int number_of_bits, int rand){
	int rest = number_of_bits % (sizeof(long) * byte_size);
	
	int padding;
	if(rest){
		padding = 1;
	}
	else {
		padding = 0;
	}
	
	int size = number_of_bits/(sizeof(unsigned long) * byte_size) + padding;
	
  unsigned long * bits;
  
  if(rand) {
    bits = malloc(sizeof(unsigned long) * size);
	int i;
    for(i = 0; i < size; i++) {
      bits[i] = random();
    }
  } else {
    bits = calloc(sizeof(unsigned long),size); //zero out the memory first
  }
	
	struct bitset * b = malloc(sizeof(struct bitset));
	b->length = size;
	b->parts = bits;
	b->number_of_usable_bits = (sizeof(unsigned long) * byte_size) * size;
	
	return b;	
}

void free_bitset(struct bitset * b){
	free(b->parts);
	free(b);
}

void print_bitset(struct bitset * b){
	int i;
	int j = 0;
	for(i = b->length - 1; i >= 0 ;i--){
		unsigned long bit_mask = -1;
		bit_mask = bit_mask >> 1; 
		bit_mask = ~bit_mask;
		long segment = b->parts[i];		
		while(bit_mask){
			printf("%u", (segment & bit_mask) > 0);	
			bit_mask = bit_mask >> 1;
      j+= 1;
      if(j == 8) {
        printf(" ");
        j=0;
      }
		}
	}
  printf("/ W: %d B: %d\n", b->length, b->number_of_usable_bits);
	//printf("\r\n");
	//printf("number of words : %d \r\n",b->length);
	//printf("number of bits : %d \r\n",b->number_of_usable_bits);
}

void set_bit(int idx, struct bitset * b){
	int segment = idx/(sizeof(long)*byte_size); 			//assumed to be floored by int division
	unsigned int seg_idx = idx % (sizeof(unsigned long)*byte_size);	//get the position in the segment
	unsigned long * l = &(b->parts[segment]);				//get the segment pointer	
	unsigned long bit = (unsigned long)1 << seg_idx;					//shift the value up to the idx		
	*l = *l | bit;
}

void reset_bit(int idx, struct bitset * b){
	int segment = idx/(sizeof(long)*byte_size); 			//assumed to be floored by int division
	unsigned int seg_idx = idx % (sizeof(unsigned long)*byte_size);	//get the position in the segment
	unsigned long * l = &(b->parts[segment]);				//get the segment pointer	
	unsigned long bit = (unsigned long)1 << seg_idx;					//shift the value up to the idx
	bit = ~bit;
	*l = *l & bit;
}

int check_bit(int idx, struct bitset * b){
	int segment = idx/(sizeof(long)*byte_size); 			//assumed to be floored by int division
	unsigned int seg_idx = idx % (sizeof(long)*byte_size);	//get the position in the segment
	unsigned long * l = &(b->parts[segment]);				//get the segment pointer	
	unsigned long bit = (unsigned long)1 << seg_idx;
	
	if(*l & bit){
		return 1;
	} else {
		return 0;
	}
}


//this can be improvied by inlining the __builtin call, might be done if an agressive -O level is used, not tried.
int popcount(struct bitset * b){
	int i;
	int l = b->length;
	int sum = 0;
	for(i = 0;i<l;i++){
		sum += __builtin_popcountl(b->parts[i]);//gcc expands this to work regardless if the cpu (target) has popcnt or not, for intel boradwell it creates a function with popcnt
	}
	return sum;
}

int popcount_sequential(struct bitset * b) {
  int sum = 0;
  int i;
  for(i=0; i < b->length; i++) {
    unsigned long bitmask = -1;
    bitmask = bitmask >> 1; 
    bitmask = ~bitmask;
	int j;
    for(j=0; j < sizeof(unsigned long) * byte_size; j++) {
      if(b->parts[i] & bitmask) {
        sum += 1;
      }
      bitmask = bitmask >> 1;
    }
  }
  return sum;
}



//returns 1 + idx of lowest bit if a bit is found, otherwise 0 if no bit is set
int find_lowest_set_bit(struct bitset * b){
	int i;
	int l = b->length;
	for(i = 0;i<l;++i){
		unsigned long s = b->parts[i];
		if(s){
			return __builtin_ffsl(s) + sizeof(unsigned long)*byte_size*i; //takes a long, returns 1 + idx if found, 0 otherwise.
		}
	}	
	return 0;
}

int find_lowest_set_bit_sequential(struct bitset * b) {
  int p = 1;
  int i;
  for(i=0; i < b->length; i++) {
    unsigned long bitmask = -1;
    bitmask = bitmask << 1;
    bitmask = ~bitmask;
	int j;
    for(j=0; j<sizeof(unsigned long) * byte_size; j++) {
      
      if( b->parts[i] & bitmask) {
        return p;
      }
      p += 1;
      bitmask = bitmask << 1;
    }
  }
  return 0;
}

int find_highest_set_bit(struct bitset * b){
	int i;
	int l = b->length;
	for(i = l - 1;i>=0;--i){
		unsigned long s = b->parts[i];
		if(s){
			int num_of_leading_zeros = __builtin_clzl(s); //takes a long and returns the number of leading zerors
			return sizeof(unsigned long)*byte_size*(i+1) - num_of_leading_zeros; // find the index of the higest index in the current word and subtract number of leading zeros and then 1 more to get to the set bit
		}
	}
	return 0;
}

int find_highest_set_bit_sequential(struct bitset * b) {
  int p = b->number_of_usable_bits;
  
  int i;
  for(i=b->length - 1; i >= 0 ; i--) {
    unsigned long bitmask = -1;
    bitmask = bitmask >> 1;
    bitmask = ~bitmask;
	int j;
    for(j=0; j<sizeof(unsigned long) * byte_size; j++) {
      
      if( b->parts[i] & bitmask) {
        return p;
      }
      p -= 1;
      bitmask = bitmask >> 1;
    }
  }
  return 0;  
}

//returns 1 + idx of second lowest lowest bit if a bit is found, otherwise 0 if no bit is set
int find_second_lowest_set_bit(struct bitset * b){
	int i;
	int l = b->length;
	int first_found = 0;
	for(i = 0;i<l;++i){
		unsigned long s = b->parts[i];
		if(s){
			if(!first_found){
				int lowest = __builtin_ffsl(s); //takes a long, returns 1 + idx if found, 0 otherwise.
				unsigned long bit = (unsigned long)1 << (lowest - 1);//shift a 1 up to the index
				bit = ~bit; //reverse it to set a zero on the index
				unsigned long word_with_out_lowest_bit = s & bit;
				s = word_with_out_lowest_bit;
			}
			
			if(s){
				return __builtin_ffsl(s) + sizeof(unsigned long)*byte_size*i;
			} else {
				first_found = 1;
			}
			
		}
	}	
	return 0;
}

struct bitset * reverse_bitset(struct bitset * b) {

	struct bitset * out = create_bitset(b->number_of_usable_bits, 0);

	int l = b->length;
	int m = sizeof(unsigned long);

	int i;
	for(i=0; i < l; i++) {
		unsigned char * p = (unsigned char *) &(b->parts[i]);
		unsigned char * q = (unsigned char *) &(out->parts[(l - 1) - i]);
		
		int j;
		for(j=0; j < m; j++) {
			q[j] = BitReverseTable256[p[(m - 1)-j]];
		}
	}

	return out;
}

void reverse_bitset_inplace(struct bitset * b) {

	int l = b->length;
	int m = sizeof(unsigned long);
  
  int i;
	for(i=0; i < (l) / 2; i++) {
    unsigned char * p = (unsigned char *) &(b->parts[i]);
		unsigned char * q = (unsigned char *) &(b->parts[(l - 1) - i]);
		int j;
		for(j=0; j < m; j++) {
      
      char swap = BitReverseTable256[p[(m - 1)-j]];
			p[(m-1)-j] = BitReverseTable256[q[j]];
      q[j] = swap;
		}
	}
  
  if(l & 1) {
    unsigned char * p = (unsigned char *) &(b->parts[i]);
		unsigned char * q = (unsigned char *) &(b->parts[(l - 1) - i]);
		int j;
		for(j=0; j < m / 2; j++) {
      
      char swap = BitReverseTable256[p[(m - 1)-j]];
			p[(m-1)-j] = BitReverseTable256[q[j]];
      q[j] = swap;
		}
  }
}

struct bitset * reverse_bitset_sequential(struct bitset * b) {
	struct bitset * out = create_bitset(b->number_of_usable_bits, 0);
	int i;
	for(i=0; i < b->length; i++) {
		unsigned long p = b->parts[i];
		unsigned long q = 0; //out->parts[(b->length - 1) - i];

		unsigned long bitmask = -1;
		bitmask = bitmask >> 1; 
		bitmask = ~bitmask;

		unsigned long writemask = -1;
		writemask = writemask << 1;
		writemask = ~writemask;
		int j;
		for(j=0; j < sizeof(unsigned long) * byte_size; j++) {
			if(p & bitmask) {
				q = q | writemask;
			}
			bitmask = bitmask >> 1;
			writemask = writemask << 1;
		}
		out->parts[(b->length - 1) -i] = q;
	}
	return out;
}


void reverse_bitset_sequential_inplace(struct bitset * b) {
	int i;
	for(i=0; i < b->length / 2; i++) {
		unsigned long p = b->parts[i];
		unsigned long q = b->parts[b->length - 1 - i]; //out->parts[(b->length - 1) - i];

    unsigned long p1 = 0;
    unsigned long q1 = 0;

		unsigned long bitmask = -1;
		bitmask = bitmask >> 1; 
		bitmask = ~bitmask;

		unsigned long writemask = -1;
		writemask = writemask << 1;
		writemask = ~writemask;
		int j;
		for(j=0; j < sizeof(unsigned long) * byte_size; j++) {
			if(p & bitmask) {
				q1 = q1 | writemask;
			}
      if(q & writemask) {
        p1 = p1 | bitmask;
      }
      
			bitmask = bitmask >> 1;
			writemask = writemask << 1;
		}
    b->parts[i] = p1;
    b->parts[b->length - 1 -i] = q1;
	}
  
  if(b->length & 1) {
    unsigned long p = b->parts[i];

    unsigned long r = 0;

		unsigned long bitmask = -1;
		bitmask = bitmask >> 1; 
		bitmask = ~bitmask;

		unsigned long writemask = -1;
		writemask = writemask << 1;
		writemask = ~writemask;
		int j;
		for(j=0; j < sizeof(unsigned long) * byte_size; j++) {
			if(p & bitmask) {
				r = r | writemask;
			}
      
			bitmask = bitmask >> 1;
			writemask = writemask << 1;
		}
    b->parts[i] = r;
  }
}

void print_char(char a) {
	int i;
	for (i = 0; i < 8; i++) {
		printf("%d", !!((a << i) & 0x80));
	}
	printf(" ");
}