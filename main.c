#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "bitset.h"
#include "variation_bitset.h"

#define cycles 1048576
#define maxsize 16384

int size=64;
struct bitset ** b;

void set_reset_test(){
	struct bitset * b = create_bitset(10,0);
	set_bit(0,b);
	set_bit(1,b);
	set_bit(2,b);
	
	print_bitset(b);
	
	reset_bit(1,b);
	free_bitset(b);
}

void check_bit_t(){
		struct bitset * b = create_bitset(10,0);


	set_bit(0,b);
	set_bit(1,b);
	set_bit(2,b);
	printf("%u\r\n",check_bit(1,b));
	
	reset_bit(1,b);
	printf("%u\r\n",check_bit(1,b));

	free_bitset(b);
}

void pop_t(){
	struct bitset * b = create_bitset(120,0);


	set_bit(0,b);
	set_bit(1,b);
	set_bit(2,b);
	
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	printf("popcount %d\r\n",popcount(b));

	free_bitset(b);
}

void pop_t_var(){
	struct bitset * b = create_bitset(120,0);


	set_bit(0,b);
	set_bit(1,b);
	set_bit(2,b);
	
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	printf("popcount %d\r\n",popcount_1(b));

	free_bitset(b);
}

void find_lsb(){
	struct bitset * b = create_bitset(120,0);
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	printf("lsb %d\r\n",find_lowest_set_bit(b));

	reset_bit(10,b);
	reset_bit(11,b);
	print_bitset(b);
	printf("lsb %d\r\n",find_lowest_set_bit(b));

	free_bitset(b);
}

void find_lsb_binary_search(){
	struct bitset * b = create_bitset(120,0);
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	printf("lsb %d\r\n",find_lowest_set_bit_binary_search_branching(b));

	reset_bit(10,b);
	reset_bit(11,b);
	print_bitset(b);
	printf("lsb %d\r\n",find_lowest_set_bit_binary_search_branching(b));

	free_bitset(b);
}


void find_second_lsb(){
	struct bitset * b = create_bitset(120,0);
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	print_bitset(b);
	printf("slsb %d\r\n",find_second_lowest_set_bit(b));

	reset_bit(10,b);
	print_bitset(b);
	printf("slsb %d\r\n",find_second_lowest_set_bit(b));

	free_bitset(b);
}

void find_hsb(){
	struct bitset * b = create_bitset(120,0);
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	printf("hsb %d\r\n",find_highest_set_bit(b));

	reset_bit(10,b);
	reset_bit(102,b);
	print_bitset(b);
	printf("hsb %d\r\n",find_highest_set_bit(b));

	free_bitset(b);
}

void reverse_t(){
	struct bitset * b = create_bitset(140,0);
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	print_bitset(b);
	struct bitset * c = reverse_bitset(b);

	print_bitset(c);
	free_bitset(c);
}

void reverse_var(){
	struct bitset * b = create_bitset(140,0);
	set_bit(10,b);
	set_bit(11,b);
	set_bit(102,b);
	print_bitset(b);
	reverse_bitset_variation(b);

	print_bitset(b);
	free_bitset(b);
}

// #####################################################################

struct timeval start_clock() {
  struct timeval ret;
  gettimeofday(&ret, NULL);
  return ret;
}

double end_clock(struct timeval start) {
  struct timeval end, res;
  gettimeofday(&end, NULL);
  timersub(&end, &start, &res);
  double time = (float)res.tv_usec / 1000000 + (float)res.tv_sec;
  //printf("Time elapsed: %ld.%06ld\n", , (long int)res.tv_usec);
  printf("Seconds: %f\n", time);
  return time;
  //
}

struct bitset ** create_random_bitsets() {
  struct bitset ** b = malloc(sizeof(struct bitset *) * cycles);
  int i;
  for(i=0; i < cycles; i++) {
    b[i] = create_bitset(size, 1);
  }
  return b;
}

void free_bitsets(struct bitset ** b) {
	int i;
	for(i=0; i < cycles; i++) {
		free_bitset(b[i]);
	}
	free(b);
}

void benchmark_popcount() {
  
  //struct bitset ** b = create_random_bitsets();
  struct timeval start;
  int i;
  double min = 9999;
  double times[] = {0,0,0};
  printf("----- POPCOUNT\n");
  
  printf("* Sequential: ");
  start = start_clock();
  for(i=0; i < cycles; i++) {
    int x = popcount_sequential(b[i]);
  }
  times[0] = end_clock(start);
  
  printf("* Optimized : ");
  start = start_clock();
  for(i=0; i < cycles; i++) {
    int x = popcount(b[i]);
  }
  times[1] = end_clock(start);
  
  printf("* Variation : ");
  start = start_clock();
  for(i=0; i < cycles; i++) {
    int x = popcount_1(b[i]);
  }
  times[2] = end_clock(start);
  
  for(i=0; i < 3; i++) {
    if(times[i] < min) {
      min = times[i];
    }
  }
  for(i=0; i < 3; i++) {
    printf("%f\t%f\n", times[i], times[i] / min);
  }
  
  //free_bitsets(b);
}

void benchmark_reverse() {
	//struct bitset ** b = create_random_bitsets();
  struct timeval start;
  int i;
  double min = 9999;
  double times[] = {0,0,0};
  printf("----- REVERSE INPLACE\n");

	printf("* Sequential: ");
	start = start_clock();
	for(i=0; i < cycles; i++) {
		reverse_bitset_sequential_inplace(b[i]);
	}
	times[0] = end_clock(start);


  printf("* Optimized : ");
	start = start_clock();
	for(i=0; i < cycles; i++) {
		reverse_bitset_inplace(b[i]);
	}
	times[1] = end_clock(start);


	printf("* Variation : ");
	start = start_clock();
	for(i=0; i < cycles; i++) {
		reverse_bitset_variation(b[i]);
	}
	times[2] = end_clock(start);


  for(i=0; i < 3; i++) {
    if(times[i] < min) {
      min = times[i];
    }
  }
  for(i=0; i < 3; i++) {
    printf("%f\t%f\n", times[i], times[i] / min);
  }

  //free_bitsets(b);
}


void benchmark_lsb() {
  
	//struct bitset ** b = create_random_bitsets();
  struct timeval start;
  int i;
  double min = 9999;
  double times[] = {0,0,0};
  printf("----- LSB\n");

	printf("* Sequential: ");
	start = start_clock();
	for(i=0; i < cycles; i++) {
		int x = find_lowest_set_bit_sequential(b[i]);
	}
	times[0] = end_clock(start);

	printf("* Optimized : ");
	start = start_clock();
	for(i=0; i < cycles; i++) {
		int x = find_lowest_set_bit(b[i]);
	}
	times[1] = end_clock(start);

	printf("* Variation : ");
	start = start_clock();
	for(i=0; i < cycles; i++) {
		int x = find_lowest_set_bit_binary_search_branching(b[i]);
	}
	times[2] = end_clock(start);

  for(i=0; i < 3; i++) {
    if(times[i] < min) {
      min = times[i];
    }
  }
  for(i=0; i < 3; i++) {
    printf("%f\t%f\n", times[i], times[i] / min);
  }
	//free_bitsets(b);
}

void benchmark_msb() {
  
	//struct bitset ** b = create_random_bitsets();
  struct timeval start;
  int i;
  double min = 9999;
  double times[] = {0,0};
  printf("----- MSB\n");

  printf("* Sequential: ");
  start = start_clock();
  for(i=0; i < cycles; i++) {
    int x = find_highest_set_bit_sequential(b[i]);
  }
  times[0] = end_clock(start);
  
  printf("* Optimized : ");
  start = start_clock();
  for(i=0; i < cycles; i++) {
    int x = find_highest_set_bit(b[i]);
  }
  times[1] = end_clock(start);
  
  for(i=0; i < 2; i++) {
    if(times[i] < min) {
      min = times[i];
    }
  }
  for(i=0; i < 2; i++) {
    printf("%f\t%f\n", times[i], times[i] / min);
  }
  
  //free_bitsets(b);
}

int main(int argc, char **argv)
{
  //size = maxsize;
  while(size <= maxsize) {
    printf("##### CYCLES / SIZE : %d / %d\n", cycles, size);
    b = create_random_bitsets();
    benchmark_lsb();
    benchmark_msb();
    benchmark_popcount();
    benchmark_reverse();
    free_bitsets(b);
    size = size * 2;
  }
	return 0;
}