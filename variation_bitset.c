#include "variation_bitset.h"
#include <stdio.h>
#include <stdlib.h>

#define byte_size 8
//returns 1 + idx of lowest bit if a bit is found, otherwise 0 if no bit is set
int find_lowest_set_bit_binary_search_branching(struct bitset * b){
	int i;
	int l = b->length;
	for(i = 0;i<l;++i){
		unsigned long s = b->parts[i];
		if(s){
			int z = 0;
	
			if ((s&0x00000000FFFFFFFF) == 0) {
				z += 32;
				s = s >> 32;
			}
			
			if ((s&0x0000FFFF) == 0) {
				z += 16;
				s = s >> 16;
			}
			if ((s&0x000000FF) == 0) {
				z += 8;
				s = s >> 8;
			}
			if ((s&0x0000000F) == 0) {
				z += 4;
				s = s >> 4;
			}
			if ((s&0x00000003) == 0) {
				z += 2;
				s = s >> 2;
			}
			if ((s&0x00000001) == 0) {
				z += 1;		
			}
			
			return z + 1 + sizeof(unsigned long)*byte_size*i; // add one to get it to be +1 of the index
		}
	}	
	
	return 0;
}


const unsigned long m1  = 0x5555555555555555; //binary: 0101...
const unsigned long m2  = 0x3333333333333333; //binary: 00110011..
const unsigned long m4  = 0x0f0f0f0f0f0f0f0f; //binary:  4 zeros,  4 ones ...
const unsigned long m8  = 0x00ff00ff00ff00ff; //binary:  8 zeros,  8 ones ...
const unsigned long m16 = 0x0000ffff0000ffff; //binary: 16 zeros, 16 ones ...
const unsigned long m32 = 0x00000000ffffffff; //binary: 32 zeros, 32 ones

int popcount_1(struct bitset * b) {
	int i;
	int l = b->length;
	int sum = 0;
	for(i = 0;i<l;++i){
		unsigned long x = b->parts[i];
		x = (x & m1 ) + ((x >>  1) & m1 ); //put count of each  2 bits into those  2 bits 
		x = (x & m2 ) + ((x >>  2) & m2 ); //put count of each  4 bits into those  4 bits 
		x = (x & m4 ) + ((x >>  4) & m4 ); //put count of each  8 bits into those  8 bits 
		x = (x & m8 ) + ((x >>  8) & m8 ); //put count of each 16 bits into those 16 bits 
		x = (x & m16) + ((x >> 16) & m16); //put count of each 32 bits into those 32 bits 
		x = (x & m32) + ((x >> 32) & m32); //put count of each 64 bits into those 64 bits 
		sum += x;
	}
	return sum;
    
}

void reverse_bitset_variation(struct bitset * b){
	const unsigned long rev_32_r = 0x00000000FFFFFFFF;
	const unsigned long rev_32_l = 0xFFFFFFFF00000000;
	const unsigned long rev_16_r = 0x0000FFFF0000FFFF;
	const unsigned long rev_16_l = 0xFFFF0000FFFF0000;
	const unsigned long rev_8_r = 0x00FF00FF00FF00FF;
	const unsigned long rev_8_l = 0xFF00FF00FF00FF00;
	const unsigned long rev_4_r = 0x0F0F0F0F0F0F0F0F;
	const unsigned long rev_4_l = 0xF0F0F0F0F0F0F0F0;
	const unsigned long rev_2_r = 0x3333333333333333;
	const unsigned long rev_2_l = 0xCCCCCCCCCCCCCCCC;
	const unsigned long rev_1_r = 0x5555555555555555;
	const unsigned long rev_1_l = 0xAAAAAAAAAAAAAAAA;

	int l = b->length;

	int i;
	for(i=0; i < l/2; i++) {
		unsigned long lower_word = b->parts[i];

		lower_word = ((lower_word&rev_1_r)<<1) | ((lower_word&rev_1_l)>>1);
		lower_word = ((lower_word&rev_2_r)<<2) | ((lower_word&rev_2_l)>>2);
		lower_word = ((lower_word&rev_4_r)<<4) | ((lower_word&rev_4_l)>>4);
		lower_word = ((lower_word&rev_8_r)<<8) | ((lower_word&rev_8_l)>>8);
		lower_word = ((lower_word&rev_16_r)<<16) | ((lower_word&rev_16_l)>>16);
		lower_word = ((lower_word&rev_32_r)<<32) | ((lower_word&rev_32_l)>>32);
		
		unsigned long upper_word = b->parts[l -1 - i];

		upper_word = ((upper_word&rev_1_r)<<1) | ((upper_word&rev_1_l)>>1);
		upper_word = ((upper_word&rev_2_r)<<2) | ((upper_word&rev_2_l)>>2);
		upper_word = ((upper_word&rev_4_r)<<4) | ((upper_word&rev_4_l)>>4);
		upper_word = ((upper_word&rev_8_r)<<8) | ((upper_word&rev_8_l)>>8);
		upper_word = ((upper_word&rev_16_r)<<16) | ((upper_word&rev_16_l)>>16);
		upper_word = ((upper_word&rev_32_r)<<32) | ((upper_word&rev_32_l)>>32);
		
		b->parts[i] = upper_word;
		b->parts[l - 1 - i] = lower_word;	

	}
	//if l is not even the loop will stop when there is one element in the middle left to invert		
	if(l&1){
		unsigned long lower_word = b->parts[i];
		lower_word = ((lower_word&rev_1_r)<<1) | ((lower_word&rev_1_l)>>1);
		lower_word = ((lower_word&rev_2_r)<<2) | ((lower_word&rev_2_l)>>2);
		lower_word = ((lower_word&rev_4_r)<<4) | ((lower_word&rev_4_l)>>4);
		lower_word = ((lower_word&rev_8_r)<<8) | ((lower_word&rev_8_l)>>8);
		lower_word = ((lower_word&rev_16_r)<<16) | ((lower_word&rev_16_l)>>16);
		lower_word = ((lower_word&rev_32_r)<<32) | ((lower_word&rev_32_l)>>32);
		b->parts[i] = lower_word;
	}
}